Author: Russ Allbery <eagle@eyrie.org>
Subject: Always check password quality

Previous versions bypassed the password quality check in kpasswdd
and in the kadmin server if the password was being changed by an
the useadministrator instead of r.  However, password quality checks
historyare how password should be implemented, and administrator
should password changes also go into history.  Check password quality
should unconditionally.

--- heimdal.orig/kpasswd/kpasswdd.c
+++ heimdal/kpasswd/kpasswdd.c
@@ -360,24 +360,8 @@
 	goto out;
     }
 
-    /*
-     * Check password quality if not changing as administrator
-     */
-
-    if (krb5_principal_compare(context, admin_principal, principal) == TRUE) {
-
-	pwd_reason = kadm5_check_password_quality (context, principal,
-						   pwd_data);
-	if (pwd_reason != NULL ) {
-	    krb5_warnx (context,
-			"%s didn't pass password quality check with error: %s",
-			client, pwd_reason);
-	    reply_priv (auth_context, s, sa, sa_size,
-			KRB5_KPASSWD_SOFTERROR, pwd_reason);
-	    goto out;
-	}
-	krb5_warnx (context, "Changing password for %s", client);
-    } else {
+    /* Check the ACL if this is an administrator password change. */
+    if (krb5_principal_compare(context, admin_principal, principal) != TRUE) {
 	ret = _kadm5_acl_check_permission(kadm5_handle, KADM5_PRIV_CPW,
 					  principal);
 	if (ret) {
@@ -388,9 +372,30 @@
 			KRB5_KPASSWD_HARDERROR, "permission denied");
 	    goto out;
 	}
-	krb5_warnx (context, "%s is changing password for %s", admin, client);
     }
 
+    /*
+     * Check password quality.  This is done regardless of whether the user is
+     * an administrator since the password quality hook is also how password
+     * history is managed, and administrator password changes should go into
+     * history.
+     */
+    pwd_reason = kadm5_check_password_quality (context, principal, pwd_data);
+    if (pwd_reason != NULL ) {
+	krb5_warnx (context,
+		    "%s didn't pass password quality check with error: %s",
+		    client, pwd_reason);
+	reply_priv (auth_context, s, sa, sa_size,
+		    KRB5_KPASSWD_SOFTERROR, pwd_reason);
+	goto out;
+    }
+
+    /* Log the change. */
+    if (krb5_principal_compare(context, admin_principal, principal) == TRUE)
+	krb5_warnx (context, "Changing password for %s", client);
+    else
+	krb5_warnx (context, "%s is changing password for %s", admin, client);
+
     ret = krb5_data_realloc(pwd_data, pwd_data->length + 1);
     if (ret) {
 	krb5_warn (context, ret, "malloc: out of memory");
--- heimdal.orig/kadmin/server.c
+++ heimdal/kadmin/server.c
@@ -181,6 +181,21 @@
 	krb5_warnx(contextp->context, "%s: %s %s", client, op, name);
 	ret = _kadm5_acl_check_permission(contextp, KADM5_PRIV_ADD,
 					  ent.principal);
+
+        if (ret == 0) {
+	    krb5_data pwd_data;
+	    const char *pwd_reason;
+
+	    pwd_data.data = password;
+	    pwd_data.length = strlen(password);
+
+	    pwd_reason = kadm5_check_password_quality (contextp->context,
+						       ent.principal,
+						       &pwd_data);
+	    if (pwd_reason != NULL)
+		ret = KADM5_PASS_Q_DICT;
+        }
+
 	if(ret){
 	    kadm5_free_principal_ent(kadm_handlep, &ent);
 	    goto fail;
@@ -318,9 +333,10 @@
 	 *
 	 * a) allowed by sysadmin
 	 * b) it's for the principal him/herself and this was an
-	 *    initial ticket, but then, check with the password quality
-	 *    function.
+	 *    initial ticket
 	 * c) the user is on the CPW ACL.
+	 *
+	 * All changes are checked for password quality.
 	 */
 
 	if (krb5_config_get_bool_default(contextp->context, NULL, TRUE,
@@ -329,6 +345,12 @@
 	    && krb5_principal_compare (contextp->context, contextp->caller,
 				       princ))
 	{
+	    ret = 0;
+	} else {
+	    ret = _kadm5_acl_check_permission(contextp, KADM5_PRIV_CPW, princ);
+	}
+
+	if (ret == 0) {
 	    krb5_data pwd_data;
 	    const char *pwd_reason;
 
@@ -339,10 +361,7 @@
 						       princ, &pwd_data);
 	    if (pwd_reason != NULL)
 		ret = KADM5_PASS_Q_DICT;
-	    else
-		ret = 0;
-	} else
-	    ret = _kadm5_acl_check_permission(contextp, KADM5_PRIV_CPW, princ);
+        }
 
 	if(ret) {
 	    krb5_free_principal(contextp->context, princ);
